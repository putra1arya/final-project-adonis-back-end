import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from 'App/Models/User'
import Mail from '@ioc:Adonis/Addons/Mail'
import Database from '@ioc:Adonis/Lucid/Database'

export default class AuthController {
/**
 *
 * @swagger
 * /api/v4/login:
 *  post:
 *    tags:
 *      - Auth
 *    summary : Auth API
 *    parameters:
 *      - name : email
 *        description: email of the user
 *        in: query
 *        type: string
 *        required: true
 *      - name : password
 *        description: password of the user
 *        in: query
 *        type: string
 *        required: true
 *    responses:
 *      '400' :
 *          description: User Tidak Ditemukan
 *      '200' :
 *          description: Get token
 *          example:
 *            type : "bearer"
 *            token : "MTY.LIPuXJHPfYOtvVWET-mhOgMc-UbGKArE33JoUVU_wDOIGV28tt_la5eHCpqr"
 *
 *
 *
 */
  public async login ({ request, auth, response }: HttpContextContract) {
    const email = request.input('email')
    const password = request.input('password')
    let user = await User.findBy('email',email)
    console.log(user)
    if(!user) return response.status(400).json({message: "User Tidak Ditemukan"})
    const token = await auth.use('api').attempt(email, password)
    return token.toJSON()
  }

/**
 *
 * @swagger
 *
 * /api/v4/register:
 *  post:
 *    summary: register API
 *    tags:
 *      - Auth
 *    parameters:
 *      - name: name
 *        description: name of the user
 *        required: true
 *      - name: email
 *        description: email of the user
 *        required: true
 *      - name: password
 *        description: password of the user
 *        required: true
 *      - name: role
 *        description: role of the user either 'user' or 'owner'
 *        required: true
 *    responses:
 *      '200' :
 *          description: Register Success, Please Verify Your Email
 *
 */
  public async register({request, response}: HttpContextContract){
    const name = request.input('name')
    const email = request.input('email')
    const password = request.input('password')
    let role = request.input('role')
    enum Role {
      user = 'user',
      owner = 'owner'
    }
    switch (role.toLocaleLowerCase()) {
      case 'owner':
        role = Role.owner
        break;
      default:
        role = Role.user
    }
    let otp = Math.floor(100000 + Math.random() * 900000)
    var current = new Date(); //'Mar 11 2015' current.getTime() = 1426060964567
    var followingDay = new Date(current.getTime() + 86400000); // + 1 day in ms
    followingDay.toLocaleDateString();

    let user = await User.create({ name,email,password,role})
    await Database.table('otp_codes').insert({
      otp_code : otp,
      user_id : user.id,
      expired_at : followingDay
    })

    await Mail.send((message) => {
      message
        .from('aryaputra@bermainapi.com')
        .to(email)
        .subject('Welcome Onboard!')
        .htmlView('email/otp_verification', { name, otp  })
    })
    return response.created({message : "Register Success, Please Verify Your Email"})

  }
  public async logout({response,auth} : HttpContextContract){
    await auth.use('api').logout()
    return response.created({message : "Success Logout"})

  }
/**
 *
 * @swagger
 *
 * /api/v4/verifikasi:
 *  post:
 *    summary: API to verify email with OTP code that send by email
 *    tags:
 *      - Auth
 *    parameters:
 *      - name: email
 *        description: email of the user
 *        required: true
 *      - name: otp code
 *        description: otp code user get after register through email
 *        required: true
 *    responses:
 *      '200' :
 *          description: Berhasil Verifikasi
 *      '400' :
 *          description: User Tidak Ditemukan
 *      '401' :
 *          description: Kode Otp Tidak ditemukan
 */
  public async otp_verifikasi({response,request} : HttpContextContract){
    let otp_code = request.input('otp_code')
    let email = request.input('email')
    let user = await User.findBy('email',email)
    if(!user) return response.status(400).json({message: 'User Tidak Ditemukan'})
    let otpCheck = await Database.query().from('otp_codes').where('otp_code',otp_code).where('user_id',user.id).first()
    if(!otpCheck) return response.status(401).json({message: 'Kode Otp Tidak Ditemukan'})
    user.isVerified = true
    await user?.save()
    return response.status(200).json({message: "Berhasil Verifikasi"})
  }
}

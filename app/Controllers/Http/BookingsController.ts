import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'
import Booking from 'App/Models/Booking'
import User from 'App/Models/User'
import Venue from 'App/Models/Venue'
import BookingValidator from 'App/Validators/BookingValidator'


export default class BookingsController {

  /**
   *
   * @swagger
   *
   * /api/v3/venue/booking/all:
   *  get:
   *    security:
   *      - bearerAuth : []
   *    tags:
   *      - Booking
   *    summary: endpoint to get all booking exist
   *    responses:
   *      '200' :
   *        description: get message Berhasil get all Booking + data
   *      '400' :
   *        description: unauthorize
   *
   */
  public async getAllBooking({response} : HttpContextContract){
    console.log('a')
    let data = await Booking.all()
    if(!data) return response.status(400).json({message: "Booking Kosong"})
    return response.status(200).json({message: "Berhasil get all Booking", data})
  }
  /**
   *@swagger
  *
  * /api/v3/venue/:id/booking:
  *  post:
  *    security:
  *      - bearerAuth : []
  *    summary: endpoint to store booking
  *    tags:
  *      - Booking
  *    parameters:
  *      - name : waktu mulai
  *        required : true
  *      - name : waktu selesai
  *        required : true
  *      - name : field id
  *        required : true
  *    responses:
  *      '200' :
  *          description: Berhasil Booking
  *      '401' :
  *          description: Unauthorize
  *
  */
  public async store({request,response,auth}:HttpContextContract){
    let inputData = await request.validate(BookingValidator)
    let bookingUser = auth.user?.id
    let {waktu_mulai,waktu_selesai,field_id} = inputData
    let user = await User.find(bookingUser)
    waktu_mulai = waktu_mulai.toString()
    waktu_selesai = waktu_selesai.toString()

    try {
      await user?.related('bookings').create({
        waktu_mulai,
        waktu_selesai,
        field_id,
        booking_user_id : bookingUser
      })
      return response.status(200).json({message: "Berhasil Booking"})
    } catch (error) {
      return response.status(400).json({message: 'Anda Telah Join di Jadwal ini'})
    }
  }
  /**
   *
   * @swagger
   * /api/v3/venue/:id/booking/:booking_id:
   *  post:
   *    security:
   *      - bearerAuth : []
   *    summary: Endpoint for join existing booking
   *    tags:
   *      - Booking
   *
   *    responses:
   *      '200' :
  *          description: Berhasil Join
  *      '401' :
  *          description: Unauthorize
   *
   */
  public async booking_field({params,auth,response}: HttpContextContract){
    let booking = await Booking.find(params.booking_id)
    let user = auth.user?.id
    await booking?.related('users').attach([user])
    return response.status(200).json({message: "Berhasil Join"})
  }

   /**
   *
   * @swagger
   * /api/v3/venue/:id/booking/:booking_id/unjoin:
   *  post:
   *    security:
   *      -bearerAuth : []
   *    summary: Endpoint for unjoin existing booking
   *    tags:
   *      - Booking
   *
   *    responses:
   *      '200' :
   *          description: Berhasil Unjoin
   *      '401' :
   *          description: Unauthorize
   *
   */
  public async cancel_join({params,auth,response}: HttpContextContract){
    let booking = await Booking.find(params.booking_id)
    await booking?.related('users').detach([auth.user?.id])
    return response.status(200).json({message: "Berhasil Unjoin"})
  }
  /**
   *
   * @swagger
   *
   * /api/v3/venue/:id/booking:
   *  get:
   *    security:
   *      - bearerAuth : []
   *    summary: Endpoint to get detail booking with user who join them
   *    tags:
   *      - Booking
   *    responses:
   *      '200' :
   *        description: get message "berhasil get data booking" + data
   *      '400' :
   *        description: unauthorize
   */
  public async detailVenueBooking({params,response}: HttpContextContract){
    let venue = await Venue.query().where('id', params.id).preload('bookings')
    console.log(venue)
    return response.status(200).json({message: "berhasil get data booking", data:venue})
  }
  /**
   *
   * @swagger
   * api/v3/venue/:id/booking/:booking_id:
   *  get:
   *    security:
   *      - bearerAuth : []
   *    summary: endpoint to get detail booking with count of users who join
   *    tags:
   *      - Booking
   *    responses:
   *      '200' :
   *        description: get message "Berhasil get Jadwal" + data
   *      '400' :
   *        description: unauthorize
   *
   * @returns
   */
  public async getJadwal({params,response}: HttpContextContract){
    let jadwal = await Booking.query().withCount('users').where('id', params.booking_id).preload('users').first()
    let rawData = JSON.stringify(jadwal)
    console.log(jadwal)
    let dataWithCount = {jumlah_peserta : jadwal?.$extras.users_count,...JSON.parse(rawData)}
    return response.status(200).json({message: "Berhasil get jadwal", data:dataWithCount})
  }
  /**
   *
   * @swagger
   * /api/v3/venue/schedule/mine:
   *  get:
   *    security:
   *      - bearerAuth : []
   *    summary: Endpoint to get my schedule
   *    tags:
   *      - Booking
   *    responses:
   *      '200':
   *        description : get message "My Schedule" + data
   *      '400':
   *        description : unauthorize
   *
   * @returns
   */
  public async mySchedule({auth,response}: HttpContextContract){
    let user = auth.user?.id
    let data = await User.query().where('id', user).preload('bookings')
    return response.status(200).json({message: "My Schedule", data})
  }
  /**
   *
   * @swagger
   * /api/v3/venue/booking/:id/update:
   *  put:
   *    security:
   *      - bearerAuth : []
   *    summary: Endpoint to update data booking
   *    tags:
   *      - Booking
   *    parameters:
   *      - name : waktu mulai
   *        required : true
   *      - name : waktu selesai
   *        required : true
   *      - name : field_id
   *        required : true
   *    responses:
   *      '200' :
   *        description : get message "Data Berhasil Diupdate" + data
   *      '400' :
   *        description : Tidak ada Data Booking Tersebut
   *      '401' :
   *        description : unauthorized
   * @returns
   */
  public async update({request,response,params} : HttpContextContract){
    const getData = await Database.from('bookings').where('id',params.id)
    if(getData.length==0){
      return response.status(400).json({message: "Tidak Ada Data Booking Tersebut"})
    }
    let inputData = await request.validate(BookingValidator)

    await Database.from('bookings').where('id',params.id).update({
        waktu_mulai : inputData.waktu_mulai.toString(),
        waktu_selesai : inputData.waktu_selesai.toString(),
        field_id : inputData.field_id,
    })
    const updated = await Database.from('bookings').where('id',params.id)
    return response.status(200).json({
      message : "Data Berhasil Di Update",
      updated
    })
  }
  /**
   *
   * @swagger
   * /api/v3/venue/booking/:id/delete:
   *  delete:
   *    security:
   *      - bearerAuth : []
   *    summary: Endpoint to delete spesific existing booking
   *    tags:
   *      - Booking
   *    responses:
   *      '200':
   *        description : Berhasil delete booking
   *      '400':
   *        description : unauthorize
   *
   * @returns
   */
  public async destroy({response, params}: HttpContextContract){
    await Database.from('bookings').where('id',params.id).delete();
    let data = await Booking.all();
    return response.status(200).json({
      message : "Data Berhasil Di Hapus",
      data
    })
  }


}

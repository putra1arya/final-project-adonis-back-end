import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'
import { schema } from '@ioc:Adonis/Core/Validator'
import Field from 'App/Models/Field'
import FieldValidator from 'App/Validators/FieldValidator'
import Venue from 'App/Models/Venue'

export default class FieldsController {

  public async getData(x){
    const data = await Database.from(x)
    return data
  }
  public async validatorInput({request}){
    enum Type {
      futsal = "Futsal",
      mini_soccer = "Mini Soccer",
      basketball = "Basketball"
    }
    const dataInput = await request.validate(FieldValidator)
    let namaVenue = dataInput.venue_name.toLocaleLowerCase()
    let [dataVenue] = await Database.query().select('id').from('venues').where('name'.toLocaleLowerCase(), namaVenue);
    if(!dataVenue) return { message : "Tidak Ada Venue Yang Bernama Tersebut"}
    switch (dataInput.type.toLocaleLowerCase()) {
      case 'futsal':
        dataInput.type = Type.futsal
        break;
      case 'mini soccer':
        dataInput.type = Type.mini_soccer
        break;
      case 'basketball':
        dataInput.type = Type.basketball
        break;
      default:
        return { message : "Pilih antara futsal, mini soccer, atau basketball "}
    }
    let name = dataInput.name
    let type = dataInput.type
    let venue_id = dataVenue.id

    return {message: 'success',name,type,venue_id}
  }
  /**
   *
   * @swagger
   * /api/v2/field:
   *  get:
   *    security:
   *      - bearerAuth: []
   *    tags:
   *      - Field
   *    summary: Get all Field Data
   *    responses:
   *      '200' :
   *        description: get message 'Berhasil get Data Field' + data
   *      '400' :
   *        description: unauthorized
   *
   * @returns
   */
  public async index({response} : HttpContextContract){
      const data = await this.getData('fields')
      return response.status(200).json({
        message: "Berhasil get Data Field",
        data
      })
  }
  /**
   *
   * @swagger
   * /api/v2/field:
   *  post:
   *    security:
   *      - bearerAuth : []
   *    tags:
   *      - Field
   *    summary: Endpoint to create new field
   *    parameters:
   *      - name: name
   *        required: true
   *      - name: type
   *        required: true
   *    responses:
   *      '200' :
   *        description: get message "Berhasil Input" + data
   *      '401' :
   *        description: Pilih antara futsal, mini soccer, atau basketball
   *      '400' :
   *        description: unauthorized
   *
   * @returns
   */
  public async v2store({response,request,params} : HttpContextContract){
    enum Type {
      futsal = "Futsal",
      mini_soccer = "Mini Soccer",
      basketball = "Basketball"
    }
    let dataInputSchema = schema.create({
      name : schema.string(),
      type : schema.string()
    })
    let validatedData = await request.validate({
      schema : dataInputSchema
    })
    switch (validatedData.type.toLocaleLowerCase()) {
      case 'futsal':
        validatedData.type = Type.futsal
        break;
      case 'mini soccer':
        validatedData.type = Type.mini_soccer
        break;
      case 'basketball':
        validatedData.type = Type.basketball
        break;
      default:
        return response.status(401).json({ message : "Pilih antara futsal, mini soccer, atau basketball "})
    }

    let field = await Field.create({
      name: validatedData.name,
      type : validatedData.type,
      venue_id : params.venue
    })
    return response.status(200).json({
      message: "Berhasil Input",
      field
    })
  }
  public async store({response,request} : HttpContextContract){
    let inputData = await this.validatorInput({request})
    if(inputData.message != 'success' ){
      return response.status(400).json({message: inputData.message})
    }
    await Database.table('fields').insert({
      name : inputData.name,
      type : inputData.type,
      venue_id : inputData.venue_id
    })
    const newData = await this.getData('fields')
    console.log(newData);
    return response.status(200).json({
      message: "Berhasil Input",
      newData
    })
  }
  /**
   *
   * @swagger
   * /api/v2/field/:id :
   *  put:
   *    security:
   *      - bearerAuth : []
   *    tags:
   *      - Field
   *    summary: Endpoint for update specific existing field
   *    parameters:
   *      - name : name
   *        required : true
   *      - name : type
   *        required : true
   *      - name : venue_id
   *        required : true
   *    responses:
   *      '200' :
   *        description : get message "Berhasil Update" + data
   *      '401' :
   *        description : Tidak ada Data Field
   *      '402' :
   *        description : Failed
   *      '400' :
   *        description : unauthorized
   * @returns
   */
  public async update({request,response,params} : HttpContextContract){
    const getData = await Database.from('fields').where('id',params.id)
    if(getData.length==0){
      return response.status(401).json({message: "Tidak Ada Data Field Tersebut"})
    }
    let inputData = await this.validatorInput({request})
    if(inputData.message != 'success' ){
      return response.status(402).json({message: inputData.message})
    }
    await Database.from('fields').where('id',params.id).update({
      name : inputData.name,
      type : inputData.type,
      venue_id : inputData.venue_id
    })
    const updated = await Database.from('fields').where('id',params.id)
    return response.status(200).json({
      message : "Data Berhasil Di Update",
      updated
    })
  }
  /**
   *
   * @swagger
   * /api/v2/field/:id :
   *  delete:
   *    security:
   *      - bearerAuth : []
   *    tags :
   *      - Field
   *    summary : To delete specific field
   *    responses:
   *      '200':
   *        description : get message "Data Berhasil Di Hapus" + data
   *      '400':
   *        description : unauthorized
   * @returns
   */
  public async destroy({response, params}: HttpContextContract){
    await Database.from('fields').where('id',params.id).delete();
    let data = await this.getData('fields');
    return response.status(200).json({
      message : "Data Berhasil Di Hapus",
      data
    })
  }



}

import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema,rules } from '@ioc:Adonis/Core/Validator'
import Database from '@ioc:Adonis/Lucid/Database'
import VenueValidator from 'App/Validators/VenueValidator'
import Venue from 'App/Models/Venue'

export default class VenuesController {

  //models
  /**
   *
   * @swagger
   * /api/v3/venue:
   *  get:
   *    security:
   *      - bearerAuth: []
   *    tags:
   *      - Venue
   *    summary: Get all Venue Data
   *    responses:
   *      '200' :
   *        description: get message 'Berhasil get Data' + data
   *      '400' :
   *        description: unauthorized
   *
   * @returns
   */
  public async v3index({response} : HttpContextContract){
    const data = await Database.query().from('venues')
    console.log(data)
    return response.status(200).json({message: "Berhasil Get Data",data});
  }
  /**
   *
   * @swagger
   * /api/v3/venue/:id:
   *  get:
   *    security:
   *      - bearerAuth : []
   *    tags:
   *      - Venue
   *    summary: Endpoint to get spesific Venue
   *    responses:
   *      '200' :
   *        description: data Venue
   *      '400' :
   *        description: unauthorized
   * @returns
   */
  public async v3show({response,params} : HttpContextContract){
    let venueDetail = await Venue.query().select('id','name','address','phone')
      .preload('fields', (query)=>{
      query.select('name','type')
    }).where('id',params.id)
    return response.status(200).json(venueDetail);
  }
  /**
   *
   * @swagger
   * /api/v3/venue/:id :
   *  put:
   *    security:
   *      - bearerAuth : []
   *    tags:
   *      - Venue
   *    summary: Endpoint for update specific existing Venue
   *    parameters:
   *      - name: name
   *        required: true
   *      - name: address
   *        required: true
   *      - name: phone
   *        required: true
   *    responses:
   *      '200' :
   *        description : get message "updated" + data
   *
   *      '400' :
   *        description : unauthorized
   * @returns
   */
  public async v3update({request,response,params}: HttpContextContract){
    const data = await request.validate(VenueValidator)
    let getdata = await Venue.findOrFail(params.id)
    getdata.name = data.name
    getdata.address = data.address
    getdata.phone = data.phone
    getdata.save()
    return response.status(200).json({message: "Updated", getdata})
  }
  /**
   *
   * @swagger
   * /api/v3/venue/:id :
   *  delete:
   *    security:
   *      - bearerAuth : []
   *    tags :
   *      - Venue
   *    summary : To delete specific venue
   *    responses:
   *      '200':
   *        description : Deleted
   *      '400':
   *        description : unauthorized
   * @returns
   */
  public async v3hapus({params,response} : HttpContextContract){
    let getdata = await Venue.findOrFail(params.id)
    await getdata.delete()
    return response.status(200).json({message : "deleted"})
  }
  /**
   *
   * @swagger
   * /api/v3/venue:
   *  post:
   *    security:
   *      - bearerAuth : []
   *    tags:
   *      - Venue
   *    summary: Endpoint to create new Venue
   *    parameters:
   *      - name: name
   *        required: true
   *      - name: address
   *        required: true
   *      - name: phone
   *        required: true
   *    responses:
   *      '200' :
   *        description: get message "Berhasil" + data
   *      '400' :
   *        description: unauthorized
   *
   * @returns
   */
  public async v3store({request,response,auth }: HttpContextContract){
    const data = await request.validate(VenueValidator)
    let user = auth.user
    let userid = user?.id
    let {name,address,phone} = data
    let venue = await Venue.create({
      name,
      address,
      phone,
      userId : userid
    })
    //console.log(venue.$isPersisted)
    return response.status(200).json({
      message: "berhasil",
      venue
    })
  }

  //query builder
  public async v2index({response} : HttpContextContract){
    const data = await Database.query().select(['id', 'name', 'address', 'phone']).from('venues')
    return response.status(200).json(data);
  }
  public async v2store({response, request} : HttpContextContract){

    const data = await request.validate(VenueValidator)
    let {name,address,phone} = data
    console.log({name,address,phone})
    const inputData = await Database.table('venues').insert({name,address,phone})
    console.log(inputData)
    return response.status(200).json({
      message: "berhasil",
      data
    })
  }
  public async v2update({response,request,params} : HttpContextContract){
    const data = await request.validate(VenueValidator)
    let {name,address,phone} = data
    let idUpdate = params.id
    await Database.from('venues').where('id',idUpdate).update({name,address,phone})
    return response.status(200).json({
      message:"berhasl Update",
      data
    })
  }
  public async v2hapus({response,params} : HttpContextContract){
    await Database.from('venues').where('id',params.id).delete()
    let data = await Database.from('venues')
    return response.status(200).json({
      message: "Deleted",
      data
    })

  }

  //no db
  public async index({response} : HttpContextContract){
    return response.status(200).json({
        message : "berhasil",
        data : { id : 1 , name:"JayaGiri", phone: 62812315232}
    })
  }
  public async store({response,request} : HttpContextContract){

    const venueSchema = schema.create({
      nama: schema.string(),
      alamat: schema.string(),
      phone: schema.string({}, [
        rules.mobile({locales:['pt-BR', 'en-IN', 'en-US','id-ID']})
      ])
    })
    const data = await request.validate({
      schema: venueSchema
    })
    let nama:string = data.nama
    let alamat:string = data.alamat
    let phone:string = data.phone
    return response.status(200).json({
      message: "berhasil",
      data : { id : 2 , nama,alamat,phone}
    })
  }

  public async booking({request, response}:HttpContextContract){
    const bookingSchema = schema.create({
      nama: schema.string(),
      venue: schema.string(),
      tanggal: schema.date({}, [
        rules.after('today')
      ])
    })
    const data = await request.validate({
      schema: bookingSchema
    })
    let nama:string = data.nama
    let venue:string = data.venue
    let tanggal = data.tanggal
    return response.status(200).json({
      message: "berhasil",
      data : { id : 2 , nama,venue,tanggal}
    })
  }

}

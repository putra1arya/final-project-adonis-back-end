import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class Role {
  public async handle ({auth,response}: HttpContextContract, next: () => Promise<void>) {
    // code for middleware goes here. ABOVE THE NEXT CALL
    let userRole = auth.user?.role
    if(userRole == 'user') return response.unauthorized({message: "Anda Tidak dapat Menggunakan Fitur ini Karena Bukan Owner"})
    await next()
  }
}

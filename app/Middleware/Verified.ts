import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from 'App/Models/User'

export default class Verified {
  public async handle ({request,response}: HttpContextContract, next: () => Promise<void>) {
    // code for middleware goes here. ABOVE THE NEXT CALL
    let user = await User.findBy('email',request.input('email'))
    if(user.isVerified == 0) return response.unauthorized({message : 'Belum terverifikasi'})
    await next()
  }
}

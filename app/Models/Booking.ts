import { DateTime } from 'luxon'
import { BaseModel, column,belongsTo,BelongsTo, manyToMany, ManyToMany, computed } from '@ioc:Adonis/Lucid/Orm'
import Field from './Field'
import User from './User'
import Database from '@ioc:Adonis/Lucid/Database'

export default class Booking extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public waktu_mulai : DateTime

  @column()
  public waktu_selesai : DateTime

  @column()
  public field_id : number

  @column()
  public booking_user_id : number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @belongsTo(()=> Field, {
    foreignKey : 'field_id'
  } )
  public field : BelongsTo <typeof Field>

  @belongsTo(()=> User, {
    foreignKey : 'user_id'
  })
  public user : BelongsTo <typeof User>

  @manyToMany(()=> User, {
    localKey: 'id',
    pivotForeignKey: 'booking_id',
    relatedKey: 'id',
    pivotRelatedForeignKey: 'user_id',
  })
  public users: ManyToMany <typeof User>

}

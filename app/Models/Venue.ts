import { DateTime } from 'luxon'
import { BaseModel, column, hasMany, HasMany,belongsTo,BelongsTo,hasManyThrough,HasManyThrough} from '@ioc:Adonis/Lucid/Orm'
import Field from './Field'
import User from './User'
import Booking from './Booking'

export default class Venue extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column()
  public name:string
  @column()
  public address:string
  @column()
  public phone:string
  @column()
  public userId:number

  @belongsTo(()=> User)
  public user : BelongsTo <typeof User>

  @column.dateTime({
    autoCreate: true,
    serialize : (value) => value.toFormat('dd LLL yyyy')
  })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true, serialize : (value) => value.toFormat('dd LLL yyyy') })
  public updatedAt: DateTime

  @hasMany(() => Field, {
    foreignKey : 'venue_id',
  })
  public fields : HasMany<typeof Field>

  @hasManyThrough([() => Booking, () =>Field], {
    localKey: 'id',
    foreignKey: 'venue_id',
    throughLocalKey: 'id',
    throughForeignKey: 'field_id'
  })
  public bookings: HasManyThrough<typeof Booking>
}

import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Bookings extends BaseSchema {
  protected tableName = 'bookings'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.timestamp('waktu_mulai').nullable().defaultTo(this.now())
      table.timestamp('waktu_selesai').nullable().defaultTo(this.now())
      table.integer('field_id').unsigned().references('id').inTable('fields').onDelete('cascade')
      table.integer('booking_user_id').unsigned().references('id').inTable('users').onDelete('cascade')
      table.timestamps()
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}

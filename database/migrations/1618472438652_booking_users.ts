import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class BookingUsers extends BaseSchema {
  protected tableName = 'booking_user'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.primary(['user_id','booking_id'])
      table.integer('user_id').unsigned().references('id').inTable('users').onDelete('cascade')
      table.integer('booking_id').unsigned().references('id').inTable('bookings').onDelete('cascade')
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}

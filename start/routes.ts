/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes/index.ts` as follows
|
| import './cart'
| import './customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'
import HealthCheck from '@ioc:Adonis/Core/HealthCheck'

Route.get('health', async ({ response }) => {
  const report = await HealthCheck.getReport()

  return report.healthy
    ? response.ok(report)
    : response.badRequest(report)
})
// Route.get('/', async () => {
//   return { hello: 'world' }
// })

// Route.group(()=>{
//   Route.get('/','VenuesController.index');
//   Route.post('/','VenuesController.store');
//   Route.post('/booking', 'VenuesController.booking')

// }).prefix('/api/v1/venue')

// Route.group(()=>{
//   Route.get('/','VenuesController.v2index');
//   Route.post('/','VenuesController.v2store');
//   Route.put('/:id','VenuesController.v2update');
//   Route.delete('/:id','VenuesController.v2hapus')

// }).prefix('/api/v2/venue')

Route.group(()=>{
  Route.get('/', 'FieldsController.index')
  Route.post('/', 'FieldsController.v2store').middleware(['role']);
  Route.put('/:id', 'FieldsController.update').middleware(['role']);
  Route.delete('/:id', 'FieldsController.destroy').middleware(['role']);
}).prefix('/api/v2/field').middleware('auth')

Route.group(()=>{
  Route.get('/','VenuesController.v3index');
  Route.post('/','VenuesController.v3store').middleware(['role']);
  Route.put('/:id','VenuesController.v3update').middleware(['role']);
  Route.delete('/:id','VenuesController.v3hapus').middleware(['role']);
  Route.get('/:id', 'VenuesController.v3show')

  Route.post('/:id/booking','BookingsController.store')
  Route.post('/:id/booking/:booking_id','BookingsController.booking_field')
  Route.post('/:id/booking/:booking_id/unjoin','BookingsController.cancel_join')
  Route.get('/:id/booking','BookingsController.detailVenueBooking')
  Route.get('/:id/booking/:booking_id','BookingsController.getJadwal')
  Route.get('/booking/all','BookingsController.getAllBooking')
  Route.put('/booking/:id/update', 'BookingsController.update')
  Route.delete('/booking/:id/delete','BookingsController.destroy')


  Route.get('/schedule/mine', 'BookingsController.mySchedule')
}).prefix('/api/v3/venue').middleware('auth')

Route.group(()=>{
  Route.post('/login', 'AuthController.login').middleware('verified')
  Route.post('/register', 'AuthController.register')
  Route.post('/verifikasi', 'AuthController.otp_verifikasi')
}).prefix('/api/v4')



